# Script Runner backend

[![Build Status](https://jenkins.alatvala.fi/job/Oy%20Aimo%20Latvala%20Ab/job/script-webhook-frontend/job/master/badge/icon)](https://jenkins.alatvala.fi/job/Oy%20Aimo%20Latvala%20Ab/job/script-webhook-frontend/job/master/badge/icon)
[![Quality Gate](https://sonar.alatvala.fi/api/badges/gate?key=SCRIPT-RUNNER-FRONTEND)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)
[![Lines](https://sonar.alatvala.fi/api/badges/measure?key=SCRIPT-RUNNER-FRONTEND&metric=lines)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)
[![Percentage of comments](https://sonar.alatvala.fi/api/badges/measure?key=SCRIPT-RUNNER-FRONTEND&metric=comment_lines_density)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)
[![Complexity to function](https://sonar.alatvala.fi/api/badges/measure?key=SCRIPT-RUNNER-FRONTEND&metric=function_complexity)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)
[![Test errors](https://sonar.alatvala.fi/api/badges/measure?key=SCRIPT-RUNNER-FRONTEND&metric=test_errors)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)
[![Test failures](https://sonar.alatvala.fi/api/badges/measure?key=SCRIPT-RUNNER-FRONTEND&metric=test_failures)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)
[![Test success density](https://sonar.alatvala.fi/api/badges/measure?key=SCRIPT-RUNNER-FRONTEND&metric=test_success_density)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)
[![Unti Test Coverage](https://sonar.alatvala.fi/api/badges/measure?key=SCRIPT-RUNNER-FRONTEND&metric=coverage)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)
[![Duplicated Lines Density](https://sonar.alatvala.fi/api/badges/measure?key=SCRIPT-RUNNER-FRONTEND&metric=duplicated_lines_density)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)
[![Blocker Violations](https://sonar.alatvala.fi/api/badges/measure?key=SCRIPT-RUNNER-FRONTEND&metric=blocker_violations)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)
[![Critical Violations](https://sonar.alatvala.fi/api/badges/measure?key=SCRIPT-RUNNER-FRONTEND&metric=critical_violations)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)
[![Code Smells](https://sonar.alatvala.fi/api/badges/measure?key=SCRIPT-RUNNER-FRONTEND&metric=code_smells)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)
[![Bugs](https://sonar.alatvala.fi/api/badges/measure?key=SCRIPT-RUNNER-FRONTEND&metric=bugs)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)
[![Vulnerabilities](https://sonar.alatvala.fi/api/badges/measure?key=SCRIPT-RUNNER-FRONTEND&metric=vulnerabilities)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)
[![Technical Debt Ratio](https://sonar.alatvala.fi/api/badges/measure?key=SCRIPT-RUNNER-FRONTEND&metric=sqale_debt_ratio)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)
[![New Maintainability Rating](https://sonar.alatvala.fi/api/badges/measure?key=SCRIPT-RUNNER-FRONTEND&metric=new_maintainability_rating)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)
[![New Reliability Rating](https://sonar.alatvala.fi/api/badges/measure?key=SCRIPT-RUNNER-FRONTEND&metric=new_reliability_rating)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)
[![New Security Rating](https://sonar.alatvala.fi/api/badges/measure?key=SCRIPT-RUNNER-FRONTEND&metric=new_security_rating)](https://sonar.alatvala.fi/dashboard/index/SCRIPT-RUNNER-FRONTEND)

This repository holds the code for Script Runner Frontend

## How do I get set up?

These steps primarily apply to a non-Windows platform, but it is absolutely possible developing on Windows too.

* Have [yarn installed](https://yarnpkg.com/lang/en/docs/install).
* Have [git installed](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
* (Optional) Have [Docker CE installed](https://docs.docker.com/install/#supported-platforms)
* (Optional, but highly recommended) Configure your editor of choice to use [ESLint](https://eslint.org/docs/user-guide/getting-started).
    * For ([Neo](https://github.com/neovim/neovim/wiki/Installing-Neovim))[Vim](https://www.vim.org/download.php) (non-neo >=8), using [Worp/Ale](https://github.com/w0rp/ale#3-installation) is highly recommended. For first time vim users, using this in conjunction with [Vim Bootstrap](https://vim-bootstrap.com/) might be a good start.
    * For [Visual Studio Code](https://code.visualstudio.com/Download), using [ESLint from the marketplace](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) is highly recommended.
    * For emacs users, `> using emacs`
* Clone this repository i.e.

* Install project dependencies i.e.

```sh
yarn install
```

* Start the appliaction, defaults to `0.0.0.0:3000` which can be accessed by visiting [localhost:3000](http://localhost:3000) i.e.

```sh
yarn start
```

## CI/CD

When making pull requests and branches, [Jenkins](https://jenkins.io/) will run an automated CI/CD pipeline, using technologies such as [SonarQube](https://sonar.alatvala.fi). This assures a baseline for code quality. Jenkins delivers the [CI/CD Pipeline](https://www.edureka.co/blog/ci-cd-pipeline/) results to BitBucket, and this will assist in preventing the merging of suboptimal code into protected branches.

### Jenkins Pipeline

This project has a CI/CD pipeline.


## Authors

[Axel Latvala](https://alatvala.fi)
