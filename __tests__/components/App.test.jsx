import React from 'react';
import { shallow } from 'enzyme';
import App from '../../src/components/App';

import consoleOutObj from '../../src/util/consoleOutObj';
import {
    OUTPUT_SYSTEM,
    OUTPUT_STDOUT,
    OUTPUT_STDERR,
} from '../../src/constants/';

import {
    WS_STATE_UNKNOWN,
} from '../../src/constants/websocket';

import {
    DEFAULT_WS_PROTO,
    DEFAULT_WS_DOMAIN,
    DEFAULT_WS_PORT,
    DEFAULT_WS_PATH,
} from '../../src/constants/defaults';

describe('<App />', () => {

    it('should render without crasahing', () => {
        shallow(
            <App
                connectWebsocket={jest.fn()}
                disconnectWebsocket={jest.fn()}
                onAuthTokenChange={jest.fn()}
                consoleOut={[
                    consoleOutObj('Some stdout text', OUTPUT_STDOUT),
                    consoleOutObj('Some stderr text', OUTPUT_STDERR),
                    consoleOutObj('Some system text', OUTPUT_SYSTEM),
                ]}
                wsState={WS_STATE_UNKNOWN}
                wsAddress={`${DEFAULT_WS_PROTO}://${DEFAULT_WS_DOMAIN}:${DEFAULT_WS_PORT}/${DEFAULT_WS_PATH}`}
                onWsAddressChange={jest.fn()}
                onMounted={jest.fn()}
                runTrigger={jest.fn()}
                runNukeTrigger={jest.fn()}
            />
        );
    });

});
