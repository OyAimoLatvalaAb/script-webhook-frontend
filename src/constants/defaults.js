export const
    DEFAULT_WS_PROTO = 'wss',
    DEFAULT_WS_DOMAIN = 'script-webhook-backend.esovellus.fi',
    DEFAULT_WS_PORT = '',
    DEFAULT_WS_PATH = '';
