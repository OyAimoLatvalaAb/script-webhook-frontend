export const OUTPUT_SYSTEM = Symbol();
export const OUTPUT_STDOUT = Symbol();
export const OUTPUT_STDERR = Symbol();
export const TRIGGER_MODE_NUKE = Symbol();
export const TRIGGER_MODE_NORMAL = Symbol();
