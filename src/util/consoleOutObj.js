import hashCode from './hashCode';

const consoleOutObj = (str, type) => ({
    id: hashCode(`${new Date().getMilliseconds()}-${str}`),
    text: str,
    type,
});

export default consoleOutObj;
