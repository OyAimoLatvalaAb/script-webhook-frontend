import {
    OUTPUT_SYSTEM,
} from '../constants/';

import consoleOutObj from '../util/consoleOutObj';

import {
    outputModeMap,
    BACKEND_EVENT_SHELLEXIT,
    BACKEND_EVENT_SHELLERROR,
    BACKEND_EVENT_OUTPUT,
    BACKEND_EVENT_STATUS,
} from '../api/backend';

const consoleOutFromMessage = (message) => {
    let parsedJson;
    try {
        parsedJson = JSON.parse(message);
    } catch(e) {
        return consoleOutObj('ERROR PARSING MESSAGE', OUTPUT_SYSTEM);
    }
    const { event } = parsedJson;
    if(event === BACKEND_EVENT_SHELLEXIT) {
        return consoleOutObj(`Shell exited with code ${parsedJson.code}`, OUTPUT_SYSTEM);
    } else if (event === BACKEND_EVENT_SHELLERROR) {
        return consoleOutObj(`Error spawning shell: ${parsedJson.err.message}`, OUTPUT_SYSTEM);
    } else if (event === BACKEND_EVENT_OUTPUT) {
        return consoleOutObj(parsedJson.output, outputModeMap[parsedJson.mode]);
    } else if (event === BACKEND_EVENT_STATUS) {
        return consoleOutObj(parsedJson.message, OUTPUT_SYSTEM);
    }
};

export default consoleOutFromMessage;
