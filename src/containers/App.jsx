import { connect } from 'react-redux';

import App from '../components/App';

import {
    runTrigger,
    connectWebsocket,
    disconnectWebsocket,
    onAuthTokenChange,
    setWsAddress,
    onBoot,
} from '../actions/';

import {
    TRIGGER_MODE_NORMAL,
    TRIGGER_MODE_NUKE,
} from '../constants/';

const mapStateToProps = (state, ownProps) => ({
    ...ownProps,
    busy: state.root.busy,
    authToken: state.root.authToken,
    wsAddress: state.root.wsAddress,
    consoleOut: state.root.consoleOut,
    wsState: state.root.wsState,
    shellCommand: state.root.shellCommand,
});

const mapDispatchToProps = (dispatch) => ({
    onAuthTokenChange: (e) => dispatch(onAuthTokenChange(e.target.value)),
    connectWebsocket: () => dispatch(connectWebsocket()),
    disconnectWebsocket: () => dispatch(disconnectWebsocket()),
    onWsAddressChange: (e) => dispatch(setWsAddress(e.target.value)),
    runTrigger: () => dispatch(runTrigger(TRIGGER_MODE_NORMAL)),
    runNukeTrigger: () => dispatch(runTrigger(TRIGGER_MODE_NUKE)),
    onMounted: () => dispatch(onBoot()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
