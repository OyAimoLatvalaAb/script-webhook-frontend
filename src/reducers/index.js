import {
    DEFAULT_WS_PROTO,
    DEFAULT_WS_DOMAIN,
    DEFAULT_WS_PORT,
    DEFAULT_WS_PATH,
} from '../constants/defaults';

import {
    WS_EVENT,
    AUTH_TOKEN_CHANGED,
    ADD_CONSOLE_OUTPUT,
    SET_WS_ADDRESS,
    SET_BUSY,
    SET_VACANT,
} from '../constants/actions';

import {
    WS_STATE_DISCONNECTED,
} from '../constants/websocket';

import eventReducer from '../reducers/eventReducer';

const initialState = {
    busy: false,
    authToken: '',
    consoleOut: [
    ],
    ws: null,
    wsState: WS_STATE_DISCONNECTED,
    wsAddress: `${DEFAULT_WS_PROTO}://${DEFAULT_WS_DOMAIN}:${DEFAULT_WS_PORT}/${DEFAULT_WS_PATH}`,
    shellCommand: '',
};

const rootReducer = (state = initialState, action) => {
    if(action.type === WS_EVENT) {
        return eventReducer(state, action.event);
    } else if(action.type === AUTH_TOKEN_CHANGED) {
        return Object.assign({}, state, {
            authToken: action.authToken,
        });
    } else if(action.type === ADD_CONSOLE_OUTPUT) {
        return Object.assign({}, state, {
            consoleOut: [...state.consoleOut, action.data],
        });
    } else if(action.type === SET_WS_ADDRESS) {
        return Object.assign({}, state, {
            wsAddress: action.data,
        });
    } else if (action.type === SET_BUSY) {
        return Object.assign({}, state, {
            busy: true,
        });
    } else if (action.type === SET_VACANT) {
        return Object.assign({}, state, {
            busy: false,
        });
    }
    return state;
};

export default rootReducer;
