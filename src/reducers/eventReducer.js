import {
    WS_STATE_CHANGE,
    WS_STATE_CONNECTING,
    WS_STATE_CONNECTED,
    WS_STATE_DISCONNECTED,
    WS_STATE_DISCONNECTING,
} from '../constants/websocket';

import {
    WS_EVT_ERROR,
    WS_EVT_MESSAGE,
    WS_EVT_PING,
    WS_EVT_PONG,
    WS_EVT_UNEXPECTED_RESPONSE,
    WS_EVT_UPGRADE,
} from '../constants/events';

import {
    OUTPUT_SYSTEM,
} from '../constants/';

import consoleOutFromMessage from '../util/consoleOutFromMessage';
import consoleOutObj from '../util/consoleOutObj';

const eventReducer = (state, event) => {
    const newState = Object.assign({}, state);
    newState.wsError = null;
    if(event.type === WS_STATE_CHANGE) {
        if(event.state === WS_STATE_CONNECTING) {
            newState.ws = event.ws;
            newState.consoleOut = [
                ...state.consoleOut,
                consoleOutObj('Connecting WebSocket...', OUTPUT_SYSTEM),
            ];
        } else if(event.state === WS_STATE_DISCONNECTED) {
            newState.ws = null;
            let message = 'Disconnected.';
            if(state.wsState === WS_STATE_CONNECTING) {
                message = 'Failed connecting WebSocket.';
            }
            newState.consoleOut = [
                ...state.consoleOut,
                consoleOutObj(message, OUTPUT_SYSTEM),
            ];
        } else if (event.state === WS_STATE_DISCONNECTING) {
            newState.consoleOut = [
                ...state.consoleOut,
                consoleOutObj('Disconnecting WebSocket...', OUTPUT_SYSTEM),
            ];
        } else if(event.state === WS_STATE_CONNECTED) {
            newState.consoleOut = [
                ...state.consoleOut,
                consoleOutObj('Connected.', OUTPUT_SYSTEM),
            ];
        }
        newState.wsState = event.state;
    } else if (event.type === WS_EVT_ERROR) {
        newState.wsError = event.error.message;
        newState.consoleOut = [
            ...state.consoleOut,
            consoleOutObj(`WebSocket error: '${event.error.message || 'unknown'}'`, OUTPUT_SYSTEM),
        ];
    } else if(event.type === WS_EVT_MESSAGE) {
        newState.consoleOut = [
            ...state.consoleOut,
            consoleOutFromMessage(event.data.data),
        ];
    } else if(event.type === WS_EVT_PING) {
        newState.consoleOut = [
            ...state.consoleOut,
            consoleOutObj('Got ping from socket', OUTPUT_SYSTEM),
        ];
    } else if(event.type === WS_EVT_PONG) {
        newState.consoleOut = [
            ...state.consoleOut,
            consoleOutObj('Got pong from socket', OUTPUT_SYSTEM),
        ];
    } else if(event.type === WS_EVT_UNEXPECTED_RESPONSE) {
        newState.consoleOut = [
            ...state.consoleOut,
            consoleOutObj('Got unexpected response (!) from socket', OUTPUT_SYSTEM),
        ];
    } else if(event.type === WS_EVT_UPGRADE) {
        newState.consoleOut = [
            ...state.consoleOut,
            consoleOutObj('Upgrading socket', OUTPUT_SYSTEM),
        ];
    }
    return newState;
};


export default eventReducer;
