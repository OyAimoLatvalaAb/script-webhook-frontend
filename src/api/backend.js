import {
    OUTPUT_STDOUT,
    OUTPUT_STDERR,
} from '../constants/';

export const BACKEND_EVENT_SHELLEXIT = 'shellExit';
export const BACKEND_EVENT_SHELLERROR = 'shellError';
export const BACKEND_EVENT_OUTPUT = 'output';
export const BACKEND_EVENT_STATUS = 'status';

export const outputModeMap = {
    'STDOUT': OUTPUT_STDOUT,
    'STDERR': OUTPUT_STDERR,
};
