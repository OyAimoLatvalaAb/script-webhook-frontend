import {
    WS_EVENT,
    AUTH_TOKEN_CHANGED,
    ADD_CONSOLE_OUTPUT,
    SET_WS_ADDRESS,
    SET_BUSY,
} from '../constants/actions';

import {
    WS_EVT_ERROR,
    WS_EVT_MESSAGE,
    WS_EVT_PING,
    WS_EVT_PONG,
    WS_EVT_UNEXPECTED_RESPONSE,
    WS_EVT_UPGRADE,
} from '../constants/events';

import {
    WS_STATE_CHANGE,
    WS_STATE_CONNECTING,
    WS_STATE_CONNECTED,
    WS_STATE_DISCONNECTING,
    WS_STATE_DISCONNECTED,
} from '../constants/websocket';

import {
    TRIGGER_MODE_NUKE,
    OUTPUT_SYSTEM,
} from '../constants';

import consoleOutObj from '../util/consoleOutObj';

export const connectWebsocket = () => (dispatch, getState) => {
    const state = getState();
    if(state.root.wsState === WS_STATE_CONNECTED) {
        return dispatch(addConsoleOutput(
            'Already connected.',
            OUTPUT_SYSTEM
        ));
    }
    const address = state.root.wsAddress;
    const ws = new WebSocket(address);
    ws.addEventListener('open', () => {
        dispatch(onWebsocketConnected());
    });
    ws.addEventListener('close', (code, reason) => {
        dispatch(onWebsocketDisconnected(code, reason));
    });
    ws.addEventListener('error', (error) => {
        dispatch(onWebsocketError(error));
    });
    ws.addEventListener('message', (data) => {
        dispatch(onWebsocketMessage(data));
    });
    ws.addEventListener('ping', (data) => {
        dispatch(onWebsocketPing(data));
    });
    ws.addEventListener('pong', (data) => {
        dispatch(onWebsocketPong(data));
    });
    ws.addEventListener('unexpected-response', (request, response) => {
        dispatch(onWebsocketUnexpectedResponse(request, response));
    });
    ws.addEventListener('upgrade', (response) => {
        dispatch(onWebsocketUpgrade(response));
    });
    return dispatch({
        type: WS_EVENT,
        event: {
            type: WS_STATE_CHANGE,
            state: WS_STATE_CONNECTING,
            ws: ws,
        },
    });
};

export const disconnectWebsocket = () => (dispatch, getState) => {
    if(getState().root.wsState !== WS_STATE_CONNECTED) {
        return dispatch(addConsoleOutput(
            'WebSocket not connected.',
            OUTPUT_SYSTEM
        ));
    }
    getState().root.ws.close();
    return dispatch({
        type: WS_EVENT,
        event: {
            type: WS_STATE_CHANGE,
            state: WS_STATE_DISCONNECTING,
        },
    });
};

export const onWebsocketConnected = () => {
    return {
        type: WS_EVENT,
        event: {
            type: WS_STATE_CHANGE,
            state: WS_STATE_CONNECTED,
        },
    };
};

export const onWebsocketDisconnected = (code, reason) => {
    return {
        type: WS_EVENT,
        event: {
            type: WS_STATE_CHANGE,
            state: WS_STATE_DISCONNECTED,
            code,
            reason,
        },
    };
};

export const onWebsocketError = (error) => {
    return {
        type: WS_EVENT,
        event: {
            type: WS_EVT_ERROR,
            error: error,
        },
    };
};

export const onWebsocketMessage = (data) => {
    return {
        type: WS_EVENT,
        event: {
            type: WS_EVT_MESSAGE,
            data,
        },
    };
};

export const onWebsocketPing = (data) => {
    return {
        type: WS_EVENT,
        event: {
            type: WS_EVT_PING,
            data,
        },
    };
};

export const onWebsocketPong = (data) => {
    return {
        type: WS_EVENT,
        event: {
            type: WS_EVT_PONG,
            data,
        },
    };
};

export const onWebsocketUnexpectedResponse = (request, response) => {
    return {
        type: WS_EVENT,
        event: {
            type: WS_EVT_UNEXPECTED_RESPONSE,
            request,
            response,
        },
    };
};

export const onWebsocketUpgrade = (response) => {
    return {
        type: WS_EVENT,
        event: {
            type: WS_EVT_UPGRADE,
            response,
        },
    };
};


export const runTrigger = (mode) => (dispatch, getState) => {
    const state = getState();
    if(state.root.wsState !== WS_STATE_CONNECTED) {
        return dispatch(addConsoleOutput(
            'Please connect WebSocket before issuing commands.',
            OUTPUT_SYSTEM
        ));
    }
    const authToken = state.root.authToken;
    state.root.ws.send(
        JSON.stringify({
            command: "trigger",
            params: {
                authToken: authToken,
                nuke: mode === TRIGGER_MODE_NUKE ? true : false,
            },
        })
    );
    return dispatch({
        type: SET_BUSY,
    });
};

export const onAuthTokenChange = (text) => ({
    type: AUTH_TOKEN_CHANGED,
    authToken: text,
});

export const addConsoleOutput = (text, type) => ({
    type: ADD_CONSOLE_OUTPUT,
    data: consoleOutObj(text, type),
});

export const setWsAddress = (newAddress) => (dispatch, getState) => {
    const state = getState();
    if(state.root.wsState !== WS_STATE_DISCONNECTED) {
        dispatch(disconnectWebsocket());
    }
    dispatch({
        type: SET_WS_ADDRESS,
        data: newAddress,
    });
};

export const onBoot = () => (dispatch) => {
    dispatch(addConsoleOutput('Application booted.', OUTPUT_SYSTEM));
};
