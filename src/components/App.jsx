import React from 'react';
import {
    func,
    string,
    arrayOf,
    shape,
    number,
} from 'prop-types';

import {
    Grid,
    Card,
    TextField,
    Button,
    InputAdornment,
} from '@material-ui/core';

import {
    Wifi,
    WifiOff,
    HourglassEmpty,
    HelpOutline,
} from '@material-ui/icons';

import {
    OUTPUT_SYSTEM,
    OUTPUT_STDOUT,
    OUTPUT_STDERR,
} from '../constants/';

import {
    WS_STATE_CONNECTING,
    WS_STATE_CONNECTED,
    WS_STATE_DISCONNECTING,
    WS_STATE_DISCONNECTED,
} from '../constants/websocket';

const style = {
    body: {
        fontFamily: 'Roboto',
    },
};
// const useStyles = makeStyles((theme) => ({
//     layout: {
//         width: 'auto',
//         marginLeft: theme.spacing(2),
//         marginRight: theme.spacing(2),
//         [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
//             width: 600,
//             marginLeft: 'auto',
//             marginRight: 'auto',
//         },
//         fontFamily: 'Roboto',
//     },
//     paper: {
//         marginTop: theme.spacing(6),
//         marginBottom: theme.spacing(6),
//         padding: theme.spacing(3),
//     },
//     buttons: {
//         display: 'flex',
//         justifyContent: 'flex-end',
//     },
//     button: {
//         marginTop: theme.spacing(3),
//         marginLeft: theme.spacing(1),
//     },
// }));

class App extends React.Component {

    /**
     * @description Main application
     */
    async componentDidMount() {
        this.scrollToBottom();
        this.props.onMounted();
    }

    componentDidUpdate = () => {
        this.scrollToBottom();
    }

    scrollToBottom = () => {
        if(this.messagesEnd)
            this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    }

    static propTypes = {
        onMounted: func.isRequired,
        connectWebsocket: func.isRequired,
        disconnectWebsocket: func.isRequired,
        onAuthTokenChange: func.isRequired,
        authToken: string,
        consoleOut: arrayOf(shape({
            id: number,
            test: string,
        })).isRequired,
        wsState: string.isRequired,
        wsAddress: string.isRequired,
        onWsAddressChange: func.isRequired,
        runTrigger: func.isRequired,
        runNukeTrigger: func.isRequired,
    }

    render() {
        const consoleOutput = this.props.consoleOut.map((consoleOut) => {
            const style = {
                backgroundColor: 'inherit',
                color: 'black',
                margin: 0,
                padding: '2px 6px',
            };
            if(consoleOut.type === OUTPUT_SYSTEM) {
                style.backgroundColor = 'lightBlue';
            } else if (consoleOut.type === OUTPUT_STDOUT) {
                style.color = 'black';
            } else if(consoleOut.type === OUTPUT_STDERR) {
                style.backgroundColor = 'orange';
            }
            return (consoleOut.text
                .split('\n')
                .map((line, idx) =>
                    idx === consoleOut.text.split('\n').length - 1 && line === '' ?
                        null :
                        (<p key={consoleOut.id+'_'+idx} style={style}>{line}</p>)
                )
            );
        });
        let wsStateSymbol;
        if(this.props.wsState === WS_STATE_DISCONNECTED) {
            wsStateSymbol = (<WifiOff style={{color: 'red'}}/>);
        } else if(this.props.wsState === WS_STATE_CONNECTED) {
            wsStateSymbol = (<Wifi style={{color: 'green'}}/>);
        } else if([WS_STATE_CONNECTING, WS_STATE_DISCONNECTING].includes(this.props.wsState)) {
            wsStateSymbol = (<HourglassEmpty style={{color: 'darkOrange'}}/>);
        } else {
            wsStateSymbol = (<HelpOutline/>);
        }
        return (
            <div>
                <Grid
                    container
                    spacing={0}
                    direction='column'
                    alignItems='center'
                    style={style.body}
                >
                    <Grid item xs={12}>
                        <Card style={{
                            marginTop: '32px',
                            padding: '24px',
                        }}>
                            <h2>
                                WebShop Export/Import
                            </h2>
                            <Grid
                                container
                                direction='column'
                                alignItems='center'
                            >
                                <Grid item
                                    xs={12} sm={10}
                                    style={{ width: '100%' }}>
                                    <TextField
                                        label='WebSocket Address'
                                        onChange={this.props.onWsAddressChange}
                                        value={this.props.wsAddress}
                                        margin='normal'
                                        variant='outlined'
                                        InputProps={{
                                            endAdornment: <InputAdornment position="end">
                                                {wsStateSymbol}
                                            </InputAdornment>,
                                        }}
                                        style={{ width: '100%' }}
                                    >
                                    </TextField>
                                </Grid>
                                <Grid item
                                    xs={12} sm={10}
                                >
                                    <Card
                                        style={{
                                            height: '256px',
                                            textAlign: 'left',
                                            padding: '0',
                                            backgroundColor: '#EEE',
                                            borderBottomLeftRadius: '0',
                                            borderBottomRightRadius: '0',
                                            overflowY: 'auto',
                                        }}
                                    >
                                        <div>
                                            {consoleOutput}
                                        </div>
                                        <div style={{ float:"left", clear: "both" }}
                                            ref={(el) => { this.messagesEnd = el; }}>
                                        </div>
                                    </Card>
                                    <Card
                                        style={{
                                            padding: '16px',
                                            textAlign: 'left',
                                            borderTopLeftRadius: '0',
                                            borderTopRightRadius: '0',
                                        }}>
                                        <Grid
                                            container
                                            layout={'row'}
                                            spacing={8}
                                            alignItems='stretch'
                                        >
                                            <Grid item xs={12} sm={6} md={3}>
                                                <Button style={{ width: '100%', height: '100%' }} variant='contained' color='primary' onClick={this.props.runTrigger}>
                                                    {'Trigger normal export/import'}
                                                </Button>
                                            </Grid>
                                            <Grid item xs={12} sm={6} md={3}>
                                                <Button style={{ width: '100%', height: '100%' }} variant='contained' color='secondary' onClick={this.props.runNukeTrigger}>
                                                    {'Trigger NUKE export/import'}
                                                </Button>
                                            </Grid>
                                            <Grid item xs={12} sm={6} md={3}>
                                                <Button style={{ width: '100%', height: '100%' }} variant='contained' color='primary' onClick={this.props.connectWebsocket}>
                                                    {'Connect WebSocket'}
                                                </Button>
                                            </Grid>
                                            <Grid item xs={12} sm={6} md={3}>
                                                <Button style={{ width: '100%', height: '100%' }} variant='contained' color='secondary' onClick={this.props.disconnectWebsocket}>
                                                    {'Disconnect WebSocket'}
                                                </Button>
                                            </Grid>
                                        </Grid>
                                        <Grid container spacing={8}>
                                            <Grid item xs={12}>
                                                <TextField
                                                    label='Authentication Token'
                                                    // className={classes.textField}
                                                    onChange={this.props.onAuthTokenChange}
                                                    value={this.props.authToken}
                                                    margin='normal'
                                                    variant='outlined'
                                                    type='password'
                                                    style={{ width: '100%' }}
                                                >
                                                </TextField>
                                            </Grid>
                                        </Grid>
                                    </Card>
                                </Grid>
                            </Grid>
                        </Card>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default App;
